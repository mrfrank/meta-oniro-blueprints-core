# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

PACKAGES:append = "\
    packagegroup-thread-br \
    packagegroup-thread-client \
    "

RDEPENDS:packagegroup-thread-br = "\
    ${@bb.utils.contains("DISTRO_FEATURES", "thread-border-router", "ot-br-posix", "", d)} \
    iptables \
    tayga \
    "

RDEPENDS:packagegroup-thread-client = "\
    ${@bb.utils.contains("DISTRO_FEATURES", "thread-client", "ot-daemon", "", d)} \
    "
